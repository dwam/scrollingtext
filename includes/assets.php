<?php 

// Add scripts
function scrt_add_scripts()
{
    wp_enqueue_style( 'scrolling-text', plugins_url( '/scrollingtext/css/scrolling-text.css'));
}

// Add action
add_action( 'wp_enqueue_scripts', 'scrt_add_scripts' );