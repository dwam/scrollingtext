<?php
/**
* Adds SCRT_Widget widget.
*/
class SCRT_Widget extends WP_Widget {
   
       /**
        * Register widget with WordPress.
        */
       function __construct() {
           parent::__construct(
               'scrt_widget', // Base ID
               esc_html__( 'Scrolling Text', 'scrt_domain' ), // Name
               array( 'description' => esc_html__( 'Widget to display a scrolling text', 'scrt_domain' ), ) // Args
           );
       }
   
       /**
        * Front-end display of widget.
        *
        * @see WP_Widget::widget()
        *
        * @param array $args     Widget arguments.
        * @param array $instance Saved values from database.
        */
       public function widget( $args, $instance ) {
           echo $args['before_widget'];
           if ( ! empty( $instance['title'] ) ) {
               echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
           }

           echo '<div id="ticker-wrap"><div class="ticker">';
           $scrolling_texts = [ $instance['scrollingtext_1'],$instance['scrollingtext_2'],$instance['scrollingtext_3']];
           //print_r($scrolling_texts);

           foreach($scrolling_texts as $scrolling_text)
           {
               echo '<div class="ticker__item">' . $scrolling_text . '</div>';
           }
           echo '</div></div>';
           
           echo $args['after_widget'];
       }
   
       /**
        * Back-end widget form.
        *
        * @see WP_Widget::form()
        *
        * @param array $instance Previously saved values from database.
        */
       public function form( $instance ) {
           $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'New title', 'text_domain' );
           ?>
           <p>
           <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'text_domain' ); ?></label> 
           <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
           </p>
           <p>
           <label for="<?php echo esc_attr( $this->get_field_id( 'scrollingtext_1' ) ); ?>"><?php esc_attr_e( 'Scrolling Text 1:', 'scrt_domain' ); ?></label> 
           <textarea class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'scrollingtext_1' ) ); ?>" rows="5" cols="5" name="<?php echo esc_attr( $this->get_field_name( 'scrollingtext_1' ) ); ?>" type="text"><?php echo $instance[ 'scrollingtext_1' ] ; ?></textarea>
           </p>
           <p>
           <label for="<?php echo esc_attr( $this->get_field_id( 'scrollingtext_2' ) ); ?>"><?php esc_attr_e( 'Scrolling Text 2:', 'scrt_domain' ); ?></label> 
           <textarea class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'scrollingtext_2' ) ); ?>" rows="5" cols="5" name="<?php echo esc_attr( $this->get_field_name( 'scrollingtext_2' ) ); ?>" type="text"><?php echo $instance[ 'scrollingtext_2' ] ; ?></textarea>
           </p>
           <p>
           <label for="<?php echo esc_attr( $this->get_field_id( 'scrollingtext_3' ) ); ?>"><?php esc_attr_e( 'Scrolling Text 3:', 'scrt_domain' ); ?></label> 
           <textarea  class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'scrollingtext_3' ) ); ?>" rows="5" cols="5" name="<?php echo esc_attr( $this->get_field_name( 'scrollingtext_3' ) ); ?>" type="text" ><?php echo $instance[ 'scrollingtext_3' ] ; ?></textarea>
            </p>
          
           <?php 
       }
   
       /**
        * Sanitize widget form values as they are saved.
        *
        * @see WP_Widget::update()
        *
        * @param array $new_instance Values just sent to be saved.
        * @param array $old_instance Previously saved values from database.
        *
        * @return array Updated safe values to be saved.
        */
       public function update( $new_instance, $old_instance ) {
           $instance = $old_instance;
           $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
           $instance['scrollingtext_1'] = ( ! empty( $new_instance['scrollingtext_1'] ) ) ? $new_instance['scrollingtext_1'] : '';
           $instance['scrollingtext_2'] = ( ! empty( $new_instance['scrollingtext_2'] ) ) ? $new_instance['scrollingtext_2'] : '';
           $instance['scrollingtext_3'] = ( ! empty( $new_instance['scrollingtext_3'] ) ) ? $new_instance['scrollingtext_3'] : '';
           
           return $instance;
       }
   
   } // class SCRT_Widget