<?php
   /*
   Plugin Name: Scrolling Text  
   Plugin URI: https://www.onemorepixel.be
   Description: Creates a widget that displays a standards-compliant horizontal scrolling text.
   Version: 1.0
   Author: Dwam
   Author URI: https://www.onemorepixel.be
   License: GPL2
   */

// Exit if accessed directly
if(!defined('ABSPATH'))
{
    exit;
}

// Load assets
require_once(plugin_dir_path(__FILE__).'includes/assets.php');

// Load classes
require_once(plugin_dir_path(__FILE__).'classes/widget.php');

// Register Widget
function register_scrt()
{
    register_widget( 'SCRT_Widget' );
}

//Hook in action
add_action( 'widgets_init', 'register_scrt' );